package banco;


public class Conta {
	Cliente titular;
	double saldo;
	int numero;

	public Conta(Cliente p_cliente, int p_numero) {
		titular = p_cliente;
		numero = p_numero;
		saldo = 0;

	}

	public double verSaldo() {
		return saldo;
	}

	public boolean saque(double valor) {
		if (valor > saldo)
			return false;
		saldo -= valor;
		return true;
		}

	public boolean deposito(double valor) {
		valor += saldo;
		return true;
	}
	

}