package banco;

public class Agencia {
	int agencia = 001;
	int verConta;
	
	public Agencia(int Agencia, int VerConta) {
		this.agencia = Agencia;
		this.verConta = VerConta;
	}

	public int getAgencia() {
		return agencia;
	}

	public void setAgencia(int agencia) {
		this.agencia = agencia;
	}

	public int getVerConta() {
		return verConta;
	}

	public void setVerConta(int verConta) {
		this.verConta = verConta;
	}
	
}
