package banco;

import java.util.Scanner;

public class Principal {

	public static void main(String[] args) {
		Scanner sc = new Scanner(System.in);
		System.out.println("Digite Seu Nome: ");
		String nome = sc.nextLine();
		System.out.println("Digite seu CPF: ");
		String cpf = sc.nextLine();
		System.out.println("Digite sua data de nascimento: ");
		String nascimento = sc.nextLine();

		Cliente cliente1 = new Cliente(nome, cpf, nascimento);
		Conta conta1 = new Conta(cliente1, 1);

		System.out.println("OP��ES: \n1.Ver Saldo da Conta\n2.Sacar\n3.Depositar\n4.FIM!!!\n");
		int opcoes = sc.nextInt();
		while (opcoes < 4) {
			opcoes = sc.nextInt();

			if (opcoes == 1) {
				System.out.println("Saldo da Conta: " + conta1.verSaldo());

			} else if (opcoes == 3) {
				System.out.println("Qual o valor do Deposito:");
				double valor = sc.nextDouble();
				System.out.println("Valor Depositado: " + conta1.deposito(valor));
				opcoes = sc.nextInt();

			} else if (opcoes == 2) {
				System.out.println("Qual o valor do Saque: ");
				double valor = sc.nextDouble();
				System.out.println("Valor Sacado: " + conta1.saque(valor));
			}

		}
		System.out.println("FIM!!!");
		sc.close();
	}
}
